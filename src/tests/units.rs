use crate::{encode_decode::DerDecode, pdu::*};

#[test]
fn simple_oid_encode() {
    use std::str::FromStr;

    let oid_s = "1.3.6.1.2.1.1.2.0";
    let oid_b = [0x2b, 0x06, 0x01, 0x02, 0x01, 0x01, 0x02, 0x00];

    let oid_decoded = ObjectId::from_str(oid_s);

    assert!(oid_decoded.is_ok());

    let oid_decoded = oid_decoded.unwrap();

    assert_eq!(oid_decoded.as_bytes(), &oid_b[..]);
}

#[test]
fn complex_oid_encode() {
    use std::str::FromStr;

    let oid_s = "1.3.6.1.4.1.253.8.64.4.2.1.7.10.14130104";
    let oid_b = [
        0x2b, 0x06, 0x01, 0x04, 0x01, 0x81, 0x7d, 0x08, 0x40, 0x04, 0x02, 0x01, 0x07, 0x0a, 0x86,
        0xde, 0xb7, 0x38,
    ];

    let oid_decoded = ObjectId::from_str(oid_s);

    assert!(oid_decoded.is_ok());

    let oid_decoded = oid_decoded.unwrap();

    assert_eq!(oid_decoded.as_bytes(), &oid_b[..]);
}

#[test]
fn vlq_decode() {
    let n = 14_130_104;
    let b = [0x86, 0xde, 0xb7, 0x38];

    assert_eq!(n, decode_vlq(&b[..]).0);
}

#[test]
fn oid_decode() {
    let oid_s = "1.3.6.1.4.1.253.8.64.4.2.1.7.10.14130104";
    let oid_b = vec![
        0x2b, 0x06, 0x01, 0x04, 0x01, 0x81, 0x7d, 0x08, 0x40, 0x04, 0x02, 0x01, 0x07, 0x0a, 0x86,
        0xde, 0xb7, 0x38,
    ];

    let oid = ObjectId(oid_b);

    assert_eq!(oid_s, oid.to_string());
}

#[test]
fn oid_encode_decode() {
    use std::str::FromStr;
    let oid_s = "1.3.6.1.4.1.4115";

    let oid_b = ObjectId::from_str(oid_s).unwrap();

    assert_eq!(oid_b.to_string(), oid_s);
}

#[test]
fn value_decodes() {
    let mut cursor = &[0x43, 0x03, 0x76, 0x86, 0x11][..];
    let res = VarBindValue::decode_from(&mut cursor);
    assert!(res.is_ok());
    assert_eq!(res.unwrap(), VarBindValue::TimeTicks(7_767_569));

    let mut cursor = &[0x02, 0x01, 0x63][..];
    let res = VarBindValue::decode_from(&mut cursor);
    assert!(res.is_ok());
    assert_eq!(res.unwrap(), VarBindValue::Integer(99));

    let mut cursor = &[0x04, 0x06, b'A', b'B', b'C', b'D', b'1', b'2'][..];
    let res = VarBindValue::decode_from(&mut cursor);
    assert!(res.is_ok());
    assert_eq!(
        res.unwrap(),
        VarBindValue::OctetString(OctetString(vec![b'A', b'B', b'C', b'D', b'1', b'2']))
    );
}

#[test]
fn whole_packet() {
    use crate::pdu;
    use std::str::FromStr;

    let mut cursor = &[
        0x30, 0x31, 0x02, 0x01, 0x01, 0x04, 0x0b, 0x5b, 0x52, 0x30, 0x5f, 0x43, 0x40, 0x63, 0x74,
        0x69, 0x21, 0x5d, 0xa2, 0x1f, 0x02, 0x04, 0x2b, 0x13, 0x3f, 0x85, 0x02, 0x01, 0x00, 0x02,
        0x01, 0x00, 0x30, 0x11, 0x30, 0x0f, 0x06, 0x0a, 0x28, 0xc4, 0x62, 0x01, 0x01, 0x01, 0x01,
        0x01, 0x01, 0x00, 0x02, 0x01, 0x02,
    ][..];

    let packet = SnmpPacket::decode_from(&mut cursor).unwrap();

    assert_eq!(
        packet,
        SnmpPacket {
            community_string: OctetString::new(b"[R0_C@cti!]".to_vec()),
            payload: Pdus::Response(pdu::Pdu::new(
                722_681_733,
                pdu::ErrorStatus::NoError,
                0,
                VarBindList::new(
                    [VarBind::new(
                        ObjectId::from_str("1.0.8802.1.1.1.1.1.1.0").unwrap(),
                        VarBindValue::Integer(2)
                    )]
                    .to_vec()
                )
            )),
        }
    );
}
