use crate::{consts::*, pdu::DecodingError};
use byteorder::{BigEndian, ReadBytesExt, WriteBytesExt};
use std::{
    io::{self, Write},
    mem::size_of,
};

pub trait DerEncode {
    fn encoded_size(&self) -> usize;
    fn encode_into<W: Write>(&self, buffer: &mut W) -> std::io::Result<()>;
}

pub trait DerDecode<'b, 'a: 'b>: Sized {
    fn decode_from(buffer: &'b mut &'a [u8]) -> Result<Self, DecodingError>;
}

pub struct Length(pub u64);

impl Length {
    pub const fn new(i: u64) -> Self {
        Self(i)
    }
}

impl DerEncode for Length {
    // TODO: better pack encoding
    fn encoded_size(&self) -> usize {
        1 + if self.0 > 127 { size_of::<u64>() } else { 0 }
    }

    fn encode_into<W: Write>(&self, buffer: &mut W) -> std::io::Result<()> {
        if self.0 > 127 {
            // Long form, 8 following octets giving the actual length
            buffer.write_u8(0x88)?;
            buffer.write_u64::<BigEndian>(self.0)?;
        } else {
            // Short form
            buffer.write_u8(self.0 as u8)?;
        }
        Ok(())
    }
}

impl<'b, 'a: 'b> DerDecode<'b, 'a> for Length {
    fn decode_from(buffer: &'b mut &'a [u8]) -> Result<Self, DecodingError> {
        let ty = buffer.read_u8()?;

        if ty & MASK_LENGTH_DEFINITE == LENGTH_DEFINITE_SHORT {
            // Short form
            let len = u64::from(ty & 0x7F);
            Ok(Length(len))
        } else {
            // Long form
            let bytes_after = ty & 0x7F;

            if bytes_after > 8 || bytes_after == 0 {
                Err(DecodingError::InvalidLength(u64::from(bytes_after)))
            } else {
                Ok(Length(match bytes_after {
                    1 => u64::from(buffer.read_u8()?),
                    2 => u64::from(buffer.read_u16::<BigEndian>()?),
                    3 => u64::from(buffer.read_u24::<BigEndian>()?),
                    4 => u64::from(buffer.read_u32::<BigEndian>()?),
                    5 => {
                        u64::from(buffer.read_u32::<BigEndian>()?) << 8
                            | u64::from(buffer.read_u8()?)
                    }
                    6 => buffer.read_u48::<BigEndian>()? as u64,
                    7 => {
                        (buffer.read_u48::<BigEndian>()? as u64) << 8 | u64::from(buffer.read_u8()?)
                    }
                    8 => buffer.read_u64::<BigEndian>()? as u64,
                    _ => return Err(DecodingError::InvalidLength(u64::from(bytes_after))),
                }))
            }
        }
    }
}

pub struct Integer(pub i64);

impl From<i64> for Integer {
    fn from(i: i64) -> Integer {
        Self(i)
    }
}

impl<'b, 'a: 'b> DerDecode<'b, 'a> for Integer {
    fn decode_from(buffer: &'b mut &'a [u8]) -> Result<Self, DecodingError> {
        let type_tag = buffer.read_u8()?;
        if type_tag != ASN1_INTEGER_RAW_TAG {
            println!("Integer");
            return Err(DecodingError::InvalidTypeTag(type_tag));
        }

        let length = Length::decode_from(buffer)?;
        Ok(match length.0 {
            1 => i64::from(buffer.read_i8()?),
            2 => i64::from(buffer.read_i16::<BigEndian>()?),
            3 => i64::from(buffer.read_i24::<BigEndian>()?),
            4 => i64::from(buffer.read_i32::<BigEndian>()?),
            5 => i64::from(buffer.read_i32::<BigEndian>()?) << 8 | i64::from(buffer.read_u8()?),
            6 => buffer.read_i48::<BigEndian>()? as i64,
            7 => (buffer.read_i48::<BigEndian>()? as i64) << 8 | i64::from(buffer.read_u8()?),
            8 => buffer.read_i64::<BigEndian>()? as i64,
            l => return Err(DecodingError::InvalidLength(l)),
        }
        .into())
    }
}

impl DerEncode for u8 {
    fn encoded_size(&self) -> usize {
        Length::new(size_of::<Self>() as u64).encoded_size() + size_of::<Self>()
    }

    fn encode_into<W: Write>(&self, buffer: &mut W) -> io::Result<()> {
        Length::new(size_of::<Self>() as u64).encode_into(buffer)?;
        buffer.write_u8(*self)?;
        Ok(())
    }
}

impl DerEncode for i8 {
    fn encoded_size(&self) -> usize {
        Length::new(size_of::<Self>() as u64).encoded_size() + size_of::<Self>()
    }

    fn encode_into<W: Write>(&self, buffer: &mut W) -> io::Result<()> {
        Length::new(size_of::<Self>() as u64).encode_into(buffer)?;
        buffer.write_i8(*self)?;
        Ok(())
    }
}

impl DerEncode for u16 {
    fn encoded_size(&self) -> usize {
        Length::new(size_of::<Self>() as u64).encoded_size() + size_of::<Self>()
    }

    fn encode_into<W: Write>(&self, buffer: &mut W) -> io::Result<()> {
        Length::new(size_of::<Self>() as u64).encode_into(buffer)?;
        buffer.write_u16::<BigEndian>(*self)?;
        Ok(())
    }
}

impl DerEncode for i16 {
    fn encoded_size(&self) -> usize {
        Length::new(size_of::<Self>() as u64).encoded_size() + size_of::<Self>()
    }

    fn encode_into<W: Write>(&self, buffer: &mut W) -> io::Result<()> {
        Length::new(size_of::<Self>() as u64).encode_into(buffer)?;
        buffer.write_i16::<BigEndian>(*self)?;
        Ok(())
    }
}

impl DerEncode for u32 {
    fn encoded_size(&self) -> usize {
        Length::new(size_of::<Self>() as u64).encoded_size() + size_of::<Self>()
    }

    fn encode_into<W: Write>(&self, buffer: &mut W) -> io::Result<()> {
        Length::new(size_of::<Self>() as u64).encode_into(buffer)?;
        buffer.write_u32::<BigEndian>(*self)?;
        Ok(())
    }
}

impl DerEncode for i32 {
    fn encoded_size(&self) -> usize {
        Length::new(size_of::<Self>() as u64).encoded_size() + size_of::<Self>()
    }

    fn encode_into<W: Write>(&self, buffer: &mut W) -> io::Result<()> {
        Length::new(size_of::<Self>() as u64).encode_into(buffer)?;
        buffer.write_i32::<BigEndian>(*self)?;
        Ok(())
    }
}

impl DerEncode for u64 {
    fn encoded_size(&self) -> usize {
        Length::new(size_of::<Self>() as u64).encoded_size() + size_of::<Self>()
    }

    fn encode_into<W: Write>(&self, buffer: &mut W) -> io::Result<()> {
        Length::new(size_of::<Self>() as u64).encode_into(buffer)?;
        buffer.write_u64::<BigEndian>(*self)?;
        Ok(())
    }
}

impl DerEncode for i64 {
    fn encoded_size(&self) -> usize {
        Length::new(size_of::<Self>() as u64).encoded_size() + size_of::<Self>()
    }

    fn encode_into<W: Write>(&self, buffer: &mut W) -> io::Result<()> {
        Length::new(size_of::<Self>() as u64).encode_into(buffer)?;
        buffer.write_i64::<BigEndian>(*self)?;
        Ok(())
    }
}

impl DerEncode for [u8] {
    fn encoded_size(&self) -> usize {
        Length::new(self.len() as u64).encoded_size() + self.len()
    }

    fn encode_into<W: Write>(&self, buffer: &mut W) -> io::Result<()> {
        Length::new(self.len() as u64).encode_into(buffer)?;

        for byte in self {
            buffer.write_u8(*byte)?;
        }

        Ok(())
    }
}
