#![allow(clippy::unknown_clippy_lints)]

#[cfg(test)]
extern crate strum;

#[cfg(test)]
#[macro_use]
extern crate strum_macros;

extern crate byteorder;

pub mod builder;
mod consts;
mod encode_decode;
pub mod pdu;

#[cfg(test)]
mod tests;

pub use crate::pdu::{DecodingError, ReceivedPdu, SnmpPacket};
use encode_decode::{DerDecode, DerEncode};
use std::{
    io,
    net::{ToSocketAddrs, UdpSocket},
    time::Duration,
};

pub struct SnmpV2C {
    udp_socket: UdpSocket,
    storage: Vec<u8>,
}

impl SnmpV2C {
    pub fn new<A: ToSocketAddrs>(socket_addr: A, max_packet_size: usize) -> io::Result<SnmpV2C> {
        Ok(SnmpV2C {
            udp_socket: UdpSocket::bind(socket_addr)?,
            storage: vec![0; max_packet_size],
        })
    }

    pub fn write_raw<A: ToSocketAddrs>(&self, bytes: &[u8], addr: A) -> io::Result<usize> {
        self.udp_socket.send_to(bytes, addr)
    }

    pub fn send_snmp_packet<A: ToSocketAddrs>(
        &self,
        request: &SnmpPacket,
        addr: A,
    ) -> io::Result<usize> {
        let mut bytes = Vec::with_capacity(request.encoded_size());
        request.encode_into(&mut bytes)?;
        self.udp_socket.send_to(&bytes, addr)
    }

    pub fn recv_snmp_packet(
        &mut self,
        timeout: Option<Duration>,
    ) -> Result<ReceivedPdu, DecodingError> {
        self.udp_socket.set_read_timeout(timeout)?;

        let (len, sender) = self.udp_socket.recv_from(&mut self.storage)?;

        let decoded = ReceivedPdu {
            sender,
            packet: SnmpPacket::decode_from(&mut &self.storage[..len])?,
        };

        Ok(decoded)
    }
}
