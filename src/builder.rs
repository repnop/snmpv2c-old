use crate::pdu::{self, *};
use std::{net, str::FromStr};

pub struct GetRequestBuilder;
pub struct GetNextRequestBuilder;
//pub struct ResponseBuilder;
pub struct SetRequestBuilder;
pub struct GetBulkRequestBuilder;
pub struct InformRequestBuilder;
//pub struct SnmpV2TrapBuilder;

impl GetRequestBuilder {
    pub fn build(pdu: Pdu) -> Pdus {
        Pdus::GetRequest(pdu)
    }
}

impl GetNextRequestBuilder {
    pub fn build(pdu: Pdu) -> Pdus {
        Pdus::GetNextRequest(pdu)
    }
}

impl SetRequestBuilder {
    pub fn build(pdu: Pdu) -> Pdus {
        Pdus::SetRequest(pdu)
    }
}

impl GetBulkRequestBuilder {
    pub fn build(pdu: BulkPdu) -> Pdus {
        Pdus::GetBulkRequest(pdu)
    }
}

impl InformRequestBuilder {
    pub fn build(pdu: Pdu) -> Pdus {
        Pdus::InformRequest(pdu)
    }
}

pub struct PduBuilder {
    request_id: i32,
    variable_bindings: Option<VarBindList>,
}

impl PduBuilder {
    pub const fn new(request_id: i32) -> PduBuilder {
        PduBuilder {
            request_id,
            variable_bindings: None,
        }
    }

    pub fn with_bindings(mut self, variable_bindings: VarBindList) -> PduBuilder {
        self.variable_bindings = Some(variable_bindings);

        self
    }

    pub fn finish(self) -> Pdu {
        let request_id = self.request_id;
        let error_status = ErrorStatus::NoError;
        let error_index = 0;
        let variable_bindings = self
            .variable_bindings
            .unwrap_or_else(|| VarBindList::new(Vec::new()));

        Pdu::new(request_id, error_status, error_index, variable_bindings)
    }
}

pub struct BulkPduBuilder {
    request_id: i32,
    non_repeaters: i32,
    max_repititions: i32,
    variable_bindings: Option<VarBindList>,
}

impl BulkPduBuilder {
    pub const fn new(request_id: i32, non_repeaters: i32, max_repititions: i32) -> BulkPduBuilder {
        BulkPduBuilder {
            request_id,
            non_repeaters,
            max_repititions,
            variable_bindings: None,
        }
    }

    pub fn with_bindings(mut self, variable_bindings: VarBindList) -> BulkPduBuilder {
        self.variable_bindings = Some(variable_bindings);

        self
    }

    pub fn finish(self) -> BulkPdu {
        let request_id = self.request_id;
        let variable_bindings = self
            .variable_bindings
            .unwrap_or_else(|| VarBindList::new(Vec::new()));

        BulkPdu::new(
            request_id,
            self.non_repeaters,
            self.max_repititions,
            variable_bindings,
        )
    }
}

#[derive(Default)]
pub struct VarBindBuilder {
    vars: Vec<VarBind>,
}

impl VarBindBuilder {
    pub fn new() -> VarBindBuilder {
        Self::default()
    }

    pub fn add_binding<B: Into<VarBindValue>>(mut self, oid: &str, binding: B) -> VarBindBuilder {
        self.vars.push(VarBind::new(
            ObjectId::from_str(oid).unwrap(),
            binding.into(),
        ));
        self
    }

    pub fn finish(self) -> VarBindList {
        VarBindList::new(self.vars)
    }
}

pub struct Integer(i32);
pub struct OctetString(Vec<u8>);
pub struct Null;
pub struct IpAddress(net::Ipv4Addr);
pub struct Counter32(u32);
pub struct Unsigned32(u32);
pub struct Gauge32(u32);
pub struct TimeTicks(u32);
pub struct Counter64(u64);

impl Into<VarBindValue> for Integer {
    fn into(self) -> VarBindValue {
        VarBindValue::Integer(self.0)
    }
}

impl Into<VarBindValue> for OctetString {
    fn into(self) -> VarBindValue {
        VarBindValue::OctetString(pdu::OctetString::new(self.0))
    }
}

impl Into<VarBindValue> for Null {
    fn into(self) -> VarBindValue {
        VarBindValue::Null
    }
}

impl Into<VarBindValue> for IpAddress {
    fn into(self) -> VarBindValue {
        VarBindValue::IpAddress(self.0.octets())
    }
}

impl Into<VarBindValue> for Counter32 {
    fn into(self) -> VarBindValue {
        VarBindValue::Counter32(self.0)
    }
}

impl Into<VarBindValue> for Unsigned32 {
    fn into(self) -> VarBindValue {
        VarBindValue::Unsigned32(self.0)
    }
}

impl Into<VarBindValue> for Gauge32 {
    fn into(self) -> VarBindValue {
        VarBindValue::Gauge32(self.0)
    }
}

impl Into<VarBindValue> for Counter64 {
    fn into(self) -> VarBindValue {
        VarBindValue::Counter64(self.0)
    }
}

impl Into<VarBindValue> for TimeTicks {
    fn into(self) -> VarBindValue {
        VarBindValue::TimeTicks(self.0)
    }
}
