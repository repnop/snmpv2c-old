use crate::{
    consts::*,
    encode_decode::{DerDecode, DerEncode, Integer, Length},
};
use byteorder::{BigEndian, ReadBytesExt, WriteBytesExt};
use std::{
    fmt::{self, Debug},
    io::{self, Read, Write},
    net::SocketAddr,
};

trait ReadSliceBytes<'b, 'a: 'b> {
    fn read_slice(&'b mut self, len: usize) -> Result<&'a [u8], io::Error>;
}

impl<'b, 'a: 'b> ReadSliceBytes<'b, 'a> for &'a [u8] {
    fn read_slice(&'b mut self, len: usize) -> Result<&'a [u8], io::Error> {
        if self.len() < len {
            return Err(io::Error::new(
                io::ErrorKind::UnexpectedEof,
                "buffer not long enough",
            ));
        }

        let slice = &self[..len];
        *self = &self[len..];

        Ok(slice)
    }
}

#[derive(Debug, Clone)]
pub struct ReceivedPdu {
    pub sender: SocketAddr,
    pub packet: SnmpPacket,
}

#[derive(Debug, Clone, PartialEq)]
pub struct SnmpPacket {
    pub community_string: OctetString,
    pub payload: Pdus,
}

impl DerEncode for SnmpPacket {
    fn encoded_size(&self) -> usize {
        2 + Length::new(1).encoded_size()
            + 2
            + Length::new(self.community_string.as_bytes().len() as u64).encoded_size()
            + self.community_string.as_bytes().len()
            + self.payload.encoded_size()
    }

    fn encode_into<W: Write>(&self, buffer: &mut W) -> io::Result<()> {
        buffer.write_u8(ASN1_SEQUENCE_ID)?;

        // Version
        buffer.write_u8(ASN1_INTEGER_RAW_TAG)?;
        Length::new(1).encode_into(buffer)?;
        buffer.write_u8(0x01)?;

        buffer.write_u8(ASN1_OCTET_STRING_RAW_TAG)?;
        Length::new(self.community_string.as_bytes().len() as u64).encode_into(buffer)?;
        buffer.write_all(&self.community_string.as_bytes())?;

        self.payload.encode_into(buffer)?;
        Ok(())
    }
}

impl<'b, 'a: 'b> DerDecode<'b, 'a> for SnmpPacket {
    fn decode_from(buffer: &'b mut &'a [u8]) -> Result<Self, DecodingError> {
        let type_tag = buffer.read_u8()?;
        if type_tag != ASN1_SEQUENCE_ID {
            return Err(DecodingError::InvalidTypeTag(type_tag));
        }

        // Length
        let _ = Length::decode_from(buffer)?;
        // Version
        let _ = Integer::decode_from(buffer)?;

        let community_string = OctetString::decode_from(buffer)?;
        let payload = Pdus::decode_from(buffer)?;

        Ok(Self {
            community_string,
            payload,
        })
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum Pdus {
    GetRequest(Pdu),
    GetNextRequest(Pdu),
    Response(Pdu),
    SetRequest(Pdu),
    GetBulkRequest(BulkPdu),
    InformRequest(Pdu),
    SnmpV2Trap(Pdu),
    Report(Pdu),
}

impl<'b, 'a: 'b> DerDecode<'b, 'a> for Pdus {
    fn decode_from(buffer: &'b mut &'a [u8]) -> Result<Self, DecodingError> {
        let type_tag = buffer.read_u8()?;

        let _ = Length::decode_from(buffer)?;

        Ok(match type_tag & 0b0001_1111 {
            GET_REQUEST => Pdus::GetRequest(Pdu::decode_from(buffer)?),
            GET_NEXT_REQUEST => Pdus::GetNextRequest(Pdu::decode_from(buffer)?),
            RESPONSE => Pdus::Response(Pdu::decode_from(buffer)?),
            SET_REQUEST => Pdus::SetRequest(Pdu::decode_from(buffer)?),
            GET_BULK_REQUEST => Pdus::GetBulkRequest(BulkPdu::decode_from(buffer)?),
            INFORM_REQUEST => Pdus::InformRequest(Pdu::decode_from(buffer)?),
            SNMP_V2_TRAP => Pdus::SnmpV2Trap(Pdu::decode_from(buffer)?),
            REPORT => Pdus::Report(Pdu::decode_from(buffer)?),
            _ => return Err(DecodingError::InvalidTypeTag(type_tag)),
        })
    }
}

impl Pdus {
    pub fn get_pdu(&self) -> Option<&Pdu> {
        Some(match self {
            Pdus::GetRequest(pdu) => pdu,
            Pdus::GetNextRequest(pdu) => pdu,
            Pdus::Response(pdu) => pdu,
            Pdus::SetRequest(pdu) => pdu,
            Pdus::GetBulkRequest(_) => return None,
            Pdus::InformRequest(pdu) => pdu,
            Pdus::SnmpV2Trap(pdu) => pdu,
            Pdus::Report(pdu) => pdu,
        })
    }

    pub fn into_pdu(self) -> Option<Pdu> {
        Some(match self {
            Pdus::GetRequest(pdu) => pdu,
            Pdus::GetNextRequest(pdu) => pdu,
            Pdus::Response(pdu) => pdu,
            Pdus::SetRequest(pdu) => pdu,
            Pdus::GetBulkRequest(_) => return None,
            Pdus::InformRequest(pdu) => pdu,
            Pdus::SnmpV2Trap(pdu) => pdu,
            Pdus::Report(pdu) => pdu,
        })
    }

    pub fn get_bulk_pdu(&self) -> Option<&BulkPdu> {
        match self {
            Pdus::GetBulkRequest(pdu) => Some(pdu),
            _ => None,
        }
    }

    pub fn into_bulk_pdu(self) -> Option<BulkPdu> {
        match self {
            Pdus::GetBulkRequest(pdu) => Some(pdu),
            _ => None,
        }
    }
}

impl DerEncode for Pdus {
    fn encoded_size(&self) -> usize {
        1 + Length::new(
            self.get_pdu()
                .map(DerEncode::encoded_size)
                .unwrap_or_else(|| self.get_bulk_pdu().unwrap().encoded_size()) as u64,
        )
        .encoded_size()
            + self
                .get_pdu()
                .map(DerEncode::encoded_size)
                .unwrap_or_else(|| self.get_bulk_pdu().unwrap().encoded_size())
    }

    fn encode_into<W: Write>(&self, buffer: &mut W) -> io::Result<()> {
        let ty = match self {
            Pdus::GetRequest(_) => GET_REQUEST_PDU_ID,
            Pdus::GetNextRequest(_) => GET_NEXT_REQUEST_PDU_ID,
            Pdus::Response(_) => RESPONSE_PDU_ID,
            Pdus::SetRequest(_) => SET_REQUEST_PDU_ID,
            Pdus::GetBulkRequest(_) => GET_BULK_REQUEST_PDU_ID,
            Pdus::InformRequest(_) => INFORM_REQUEST_PDU_ID,
            Pdus::SnmpV2Trap(_) => SNMP_V2_TRAP_PDU_ID,
            Pdus::Report(_) => REPORT_PDU_ID,
        };

        buffer.write_u8(ty)?;

        match self {
            Pdus::GetRequest(pdu)
            | Pdus::GetNextRequest(pdu)
            | Pdus::Response(pdu)
            | Pdus::SetRequest(pdu)
            | Pdus::InformRequest(pdu)
            | Pdus::SnmpV2Trap(pdu)
            | Pdus::Report(pdu) => {
                Length::new(pdu.encoded_size() as u64).encode_into(buffer)?;
                pdu.encode_into(buffer)?;
            }
            Pdus::GetBulkRequest(pdu) => {
                Length::new(pdu.encoded_size() as u64).encode_into(buffer)?;
                pdu.encode_into(buffer)?;
            }
        }

        Ok(())
    }
}

#[derive(Debug)]
pub enum DecodingError {
    InvalidPduId,
    InvalidLength(u64),
    InvalidErrorStatus,
    InvalidVarBindId(u8),
    InvalidObjectSyntaxId,
    InvalidTypeTag(u8),
    IoError(io::Error),
    ValueTooLarge,
}

impl From<io::Error> for DecodingError {
    fn from(e: io::Error) -> DecodingError {
        DecodingError::IoError(e)
    }
}

impl From<std::num::TryFromIntError> for DecodingError {
    fn from(_: std::num::TryFromIntError) -> Self {
        DecodingError::ValueTooLarge
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Pdu {
    request_id: i32,
    error_status: ErrorStatus,
    error_index: i32,
    variable_bindings: VarBindList,
}

impl Pdu {
    pub const fn new(
        request_id: i32,
        error_status: ErrorStatus,
        error_index: i32,
        variable_bindings: VarBindList,
    ) -> Pdu {
        Pdu {
            request_id,
            error_status,
            error_index,
            variable_bindings,
        }
    }

    /*
    request_id: i32,
    error_status: ErrorStatus,
    error_index: i32,
    variable_bindings: VarBindList,
    */

    pub const fn request_id(&self) -> i32 {
        self.request_id
    }

    pub const fn error_status(&self) -> ErrorStatus {
        self.error_status
    }

    pub const fn error_index(&self) -> i32 {
        self.error_index
    }

    pub fn variable_bindings(&self) -> &[VarBind] {
        &self.variable_bindings.0
    }

    pub fn into_parts(self) -> (i32, i32, ErrorStatus, VarBindList) {
        (
            self.request_id,
            self.error_index,
            self.error_status,
            self.variable_bindings,
        )
    }
}

impl DerEncode for Pdu {
    fn encoded_size(&self) -> usize {
        1 + self.request_id.encoded_size()
            + 1
            + self.error_index.encoded_size()
            + self.error_status.encoded_size()
            + 1
            + Length::new(self.variable_bindings.encoded_size() as u64).encoded_size()
            + self.variable_bindings.encoded_size()
    }

    fn encode_into<W: Write>(&self, buffer: &mut W) -> io::Result<()> {
        buffer.write_u8(ASN1_INTEGER_RAW_TAG)?;
        self.request_id.encode_into(buffer)?;

        buffer.write_u8(ASN1_INTEGER_RAW_TAG)?;
        self.error_index.encode_into(buffer)?;

        self.error_status.encode_into(buffer)?;

        buffer.write_u8(ASN1_SEQUENCE_ID)?;
        Length::new(self.variable_bindings.encoded_size() as u64).encode_into(buffer)?;
        self.variable_bindings.encode_into(buffer)?;

        Ok(())
    }
}

impl<'b, 'a: 'b> DerDecode<'b, 'a> for Pdu {
    fn decode_from(buffer: &'b mut &'a [u8]) -> Result<Self, DecodingError> {
        let request_id = Integer::decode_from(buffer)?.0 as i32;
        let error_status = ErrorStatus::try_from(Integer::decode_from(buffer)?.0 as i32)?;
        let error_index = Integer::decode_from(buffer)?.0 as i32;

        let variable_bindings = VarBindList::decode_from(buffer)?;

        Ok(Pdu {
            request_id,
            error_status,
            error_index,
            variable_bindings,
        })
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct BulkPdu {
    request_id: i32,
    non_repeaters: i32,
    max_repititions: i32,
    variable_bindings: VarBindList,
}

impl BulkPdu {
    pub(crate) const fn new(
        request_id: i32,
        non_repeaters: i32,
        max_repititions: i32,
        variable_bindings: VarBindList,
    ) -> BulkPdu {
        BulkPdu {
            request_id,
            non_repeaters,
            max_repititions,
            variable_bindings,
        }
    }

    pub const fn request_id(&self) -> i32 {
        self.request_id
    }

    pub const fn non_repeaters(&self) -> i32 {
        self.non_repeaters
    }

    pub const fn max_repititions(&self) -> i32 {
        self.max_repititions
    }

    pub fn variable_bindings(&self) -> &[VarBind] {
        self.variable_bindings.variable_bindings()
    }
}

impl DerEncode for BulkPdu {
    fn encoded_size(&self) -> usize {
        1 + self.request_id.encoded_size()
            + 1
            + self.non_repeaters.encoded_size()
            + 1
            + self.max_repititions.encoded_size()
            + 1
            + Length::new(self.variable_bindings.encoded_size() as u64).encoded_size()
            + self.variable_bindings.encoded_size()
    }

    fn encode_into<W: Write>(&self, buffer: &mut W) -> io::Result<()> {
        buffer.write_u8(ASN1_INTEGER_RAW_TAG)?;
        self.request_id.encode_into(buffer)?;

        buffer.write_u8(ASN1_INTEGER_RAW_TAG)?;
        self.non_repeaters.encode_into(buffer)?;

        buffer.write_u8(ASN1_INTEGER_RAW_TAG)?;
        self.max_repititions.encode_into(buffer)?;

        buffer.write_u8(ASN1_SEQUENCE_ID)?;
        Length::new(self.variable_bindings.encoded_size() as u64).encode_into(buffer)?;
        self.variable_bindings.encode_into(buffer)?;

        Ok(())
    }
}

impl<'b, 'a: 'b> DerDecode<'b, 'a> for BulkPdu {
    fn decode_from(buffer: &'b mut &'a [u8]) -> Result<Self, DecodingError> {
        let request_id = Integer::decode_from(buffer)?.0 as i32;
        let non_repeaters = Integer::decode_from(buffer)?.0 as i32;
        let max_repititions = Integer::decode_from(buffer)?.0 as i32;
        let variable_bindings = VarBindList::decode_from(buffer)?;

        Ok(BulkPdu {
            request_id,
            non_repeaters,
            max_repititions,
            variable_bindings,
        })
    }
}

#[cfg_attr(test, derive(EnumIter))]
#[derive(Debug, Clone, Copy, PartialEq)]
#[repr(i32)]
pub enum ErrorStatus {
    NoError = 0,
    TooBig = 1,
    NoSuchName = 2,
    BadValue = 3,
    ReadOnly = 4,
    GenErr = 5,
    NoAccess = 6,
    WrongType = 7,
    WrongLength = 8,
    WrongEncoding = 9,
    WrongValue = 10,
    NoCreation = 11,
    InconsistentValue = 12,
    ResourceUnavailable = 13,
    CommitFailed = 14,
    UndoFailed = 15,
    AuthroizationError = 16,
    NotWritable = 17,
    InconsistentName = 18,
}

impl ErrorStatus {
    pub const fn as_i32(self) -> i32 {
        self as i32
    }

    pub fn try_from(n: i32) -> Result<ErrorStatus, DecodingError> {
        if n > 18 || n < 0 {
            return Err(DecodingError::InvalidErrorStatus);
        }

        Ok(unsafe { ::std::mem::transmute(n) })
    }
}

impl DerEncode for ErrorStatus {
    fn encoded_size(&self) -> usize {
        1 + self.as_i32().encoded_size()
    }

    fn encode_into<W: Write>(&self, buffer: &mut W) -> io::Result<()> {
        buffer.write_u8(ASN1_INTEGER_RAW_TAG)?;
        self.as_i32().encode_into(buffer)?;
        Ok(())
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct VarBind {
    name: ObjectId,
    value: VarBindValue,
}

impl VarBind {
    pub const fn new(name: ObjectId, value: VarBindValue) -> VarBind {
        VarBind { name, value }
    }

    pub const fn name(&self) -> &ObjectId {
        &self.name
    }

    pub const fn value(&self) -> &VarBindValue {
        &self.value
    }

    pub fn into_parts(self) -> (ObjectId, VarBindValue) {
        (self.name, self.value)
    }
}

impl DerEncode for VarBind {
    fn encoded_size(&self) -> usize {
        1 + Length::new(self.name.encoded_size() as u64 + self.value.encoded_size() as u64)
            .encoded_size()
            + self.name.encoded_size()
            + self.value.encoded_size()
    }

    fn encode_into<W: Write>(&self, buffer: &mut W) -> io::Result<()> {
        buffer.write_u8(ASN1_SEQUENCE_ID)?;
        Length::new(self.name.encoded_size() as u64 + self.value.encoded_size() as u64)
            .encode_into(buffer)?;
        self.name.encode_into(buffer)?;
        self.value.encode_into(buffer)?;
        Ok(())
    }
}

impl<'b, 'a: 'b> DerDecode<'b, 'a> for VarBind {
    fn decode_from(buffer: &'b mut &'a [u8]) -> Result<Self, DecodingError> {
        let type_tag = buffer.read_u8()?;
        if type_tag != ASN1_SEQUENCE_ID {
            return Err(DecodingError::InvalidTypeTag(type_tag));
        }

        let _ = Length::decode_from(buffer)?;
        let name = ObjectId::decode_from(buffer)?;
        let value = VarBindValue::decode_from(buffer)?;

        Ok(Self { name, value })
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct VarBindList(Vec<VarBind>);

impl VarBindList {
    pub const fn new(list: Vec<VarBind>) -> VarBindList {
        VarBindList(list)
    }

    pub fn variable_bindings(&self) -> &[VarBind] {
        &self.0
    }

    pub fn into_inner(self) -> Vec<VarBind> {
        self.0
    }
}

impl DerEncode for VarBindList {
    fn encoded_size(&self) -> usize {
        self.0.iter().fold(0, |sum, item| sum + item.encoded_size())
    }

    fn encode_into<W: Write>(&self, buffer: &mut W) -> io::Result<()> {
        for item in &self.0 {
            item.encode_into(buffer)?;
        }

        Ok(())
    }
}

impl<'b, 'a: 'b> DerDecode<'b, 'a> for VarBindList {
    fn decode_from(buffer: &'b mut &'a [u8]) -> Result<Self, DecodingError> {
        let type_tag = buffer.read_u8()?;
        if type_tag != ASN1_SEQUENCE_ID {
            return Err(DecodingError::InvalidTypeTag(type_tag));
        }

        let length = Length::decode_from(buffer)?.0;
        let mut bytes = buffer.read_slice(length as usize)?;

        let mut var_binds = Vec::with_capacity(10);

        while !bytes.is_empty() {
            var_binds.push(VarBind::decode_from(&mut bytes)?);
        }

        Ok(VarBindList(var_binds))
    }
}

#[derive(Clone, PartialEq)]
pub struct ObjectId(pub(crate) Vec<u8>);

impl ObjectId {
    pub fn as_bytes(&self) -> &[u8] {
        &self.0
    }

    pub fn to_vec_of_nums(&self) -> Vec<u64> {
        self.iter().collect()
    }

    pub fn iter(&self) -> ObjectIdIter {
        ObjectIdIter {
            bytes: &self.0,
            index: 0,
        }
    }
}

impl DerEncode for ObjectId {
    fn encoded_size(&self) -> usize {
        TAG_LENGTH + Length::new(self.0.len() as u64).encoded_size() + self.0.len()
    }

    fn encode_into<W: WriteBytesExt>(&self, buffer: &mut W) -> std::io::Result<()> {
        buffer.write_u8(ASN1_OBJECT_IDENTIFIER_RAW_TAG)?;
        Length::new(self.0.len() as u64).encode_into(buffer)?;
        for byte in self.as_bytes() {
            buffer.write_u8(*byte)?;
        }

        Ok(())
    }
}

impl<'b, 'a: 'b> DerDecode<'b, 'a> for ObjectId {
    fn decode_from(buffer: &'b mut &'a [u8]) -> Result<Self, DecodingError> {
        let type_tag = buffer.read_u8()?;
        if type_tag != ASN1_OBJECT_IDENTIFIER_RAW_TAG {
            return Err(DecodingError::InvalidTypeTag(type_tag));
        }

        let len = Length::decode_from(buffer)?.0 as usize;
        let oid_buffer = buffer.read_slice(len)?;

        Ok(ObjectId(oid_buffer.to_vec()))
    }
}

impl Debug for ObjectId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let first_oid = u32::from(self.0[0]);
        let sub_oid_1 = first_oid / 40;
        let sub_oid_2 = first_oid % 40;

        write!(f, "ObjectId({}.{}", sub_oid_1, sub_oid_2)?;

        let mut position = 1;
        let len = self.0.len();

        while position < len {
            let mut result = 0u64;

            loop {
                let byte = self.0[position];

                result = (result << 7) | u64::from(byte & !0x80);

                position += 1;

                if self.0.get(position).is_none() || byte & 0x80 != 0x80 {
                    break;
                }
            }

            write!(f, ".{}", result)?;
        }

        write!(f, ")")?;

        Ok(())
    }
}

pub struct ObjectIdIter<'a> {
    bytes: &'a [u8],
    index: usize,
}

impl<'a> Iterator for ObjectIdIter<'a> {
    type Item = u64;

    fn next(&mut self) -> Option<u64> {
        if self.index >= self.bytes.len() {
            None
        } else if self.index == 0 {
            self.index += 1;
            Some(u64::from(self.bytes[0]) / 40)
        } else if self.index == 1 {
            self.index += 1;
            Some(u64::from(self.bytes[0]) % 40)
        } else {
            let ret;

            let b = self.bytes[self.index - 1];

            if b & 0x80 == 0 {
                ret = u64::from(b);
                self.index += 1;
            } else {
                let (n, offset) = decode_vlq(&self.bytes[self.index - 1..]);
                self.index += offset;

                ret = n;
            }

            Some(ret)
        }
    }
}

use std::cmp::Ordering;
impl PartialOrd for ObjectId {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        for (n1, n2) in self.iter().zip(other.iter()) {
            if n1 != n2 {
                return n1.partial_cmp(&n2);
            }
        }

        Some(Ordering::Equal)
    }
}

#[derive(Debug)]
pub enum OidError {
    ParseError(::std::num::ParseIntError),
}

impl ToString for OidError {
    fn to_string(&self) -> String {
        String::from("Error parsing OID values.")
    }
}

impl From<::std::num::ParseIntError> for OidError {
    fn from(e: ::std::num::ParseIntError) -> OidError {
        OidError::ParseError(e)
    }
}

impl<'a> From<Vec<u8>> for ObjectId {
    fn from(oids: Vec<u8>) -> ObjectId {
        ObjectId(oids)
    }
}

impl ::std::str::FromStr for ObjectId {
    type Err = OidError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut oids = Vec::new();

        let mut parts = s.split('.');

        let first = parts.next().unwrap();

        oids.push(first.parse::<u8>()? * 40);

        if let Some(second) = parts.next() {
            oids[0] += second.parse::<u8>()?;

            for part in parts {
                let oid = part.parse::<u64>()?;

                if oid <= 127 {
                    oids.push(oid as u8);
                } else {
                    oids.extend_from_slice(&encode_single_u64(oid));
                }
            }
        }

        Ok(ObjectId(oids))
    }
}

impl ToString for ObjectId {
    fn to_string(&self) -> String {
        let mut s = String::new();

        let mut iter = self.iter();

        s.push_str(&iter.next().unwrap().to_string());

        for b in iter {
            s.push('.');
            s.push_str(&b.to_string());
        }

        s
    }
}

fn encode_single_u64(n: u64) -> Vec<u8> {
    let mut end_result = Vec::with_capacity(2);
    let mut index = 7;
    let mut sig_bit_reached = false;
    let mut mask = 0x7Fu64 << (index * 7);

    while index >= 0 {
        let mut buffer = mask & n;

        if buffer > 0 || sig_bit_reached {
            sig_bit_reached = true;
            buffer >>= index * 7;

            if index > 0 {
                buffer |= 0x80;
            }

            end_result.push(buffer as u8);
        }

        mask >>= 7;
        index -= 1;
    }

    end_result
}

pub(crate) fn decode_vlq(n: &[u8]) -> (u64, usize) {
    let mut result = 0;

    let mut len = 0;

    for &b in n {
        if b & 0x80 == 0 {
            result <<= 7;
            result |= u64::from(b);
            len += 1;
            break;
        } else {
            let b = u64::from(b & 0x7F);
            result <<= 7;
            result |= b;
            len += 1;
        }
    }

    (result, len)
}

#[derive(Clone, PartialEq)]
/// A byte string.
///
/// Note: The `ToString` impl uses `String::from_utf8_lossy` internally since
/// the function call should never fail.
pub struct OctetString(pub(crate) Vec<u8>);

impl OctetString {
    pub fn new(v: Vec<u8>) -> OctetString {
        OctetString(v)
    }

    pub fn as_bytes(&self) -> &[u8] {
        &self.0
    }

    pub fn as_str(&self) -> Result<&str, std::str::Utf8Error> {
        std::str::from_utf8(&self.0)
    }
}

impl DerEncode for OctetString {
    fn encoded_size(&self) -> usize {
        TAG_LENGTH + Length::new(self.0.len() as u64).encoded_size() + self.0.len()
    }

    fn encode_into<W: WriteBytesExt>(&self, buffer: &mut W) -> std::io::Result<()> {
        buffer.write_u8(ASN1_OCTET_STRING_RAW_TAG)?;
        Length::new(self.0.len() as u64).encode_into(buffer)?;
        for byte in self.as_bytes() {
            buffer.write_u8(*byte)?;
        }

        Ok(())
    }
}

impl<'b, 'a: 'b> DerDecode<'b, 'a> for OctetString {
    fn decode_from(buffer: &'b mut &'a [u8]) -> Result<Self, DecodingError> {
        let type_tag = buffer.read_u8()?;
        if type_tag != ASN1_OCTET_STRING_RAW_TAG {
            return Err(DecodingError::InvalidTypeTag(type_tag));
        }

        let len = Length::decode_from(buffer)?.0 as usize;
        let str_buffer = buffer.read_slice(len)?;

        Ok(OctetString(str_buffer.into()))
    }
}

impl ToString for OctetString {
    fn to_string(&self) -> String {
        String::from_utf8_lossy(&self.0).into_owned()
    }
}

impl Debug for OctetString {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "OctetString(\"")?;

        if self.0.is_empty() {
            write!(f, "<MISSING>")?;
        } else {
            let first_byte = self.0[0];

            if first_byte >= 32 && first_byte <= 127 {
                write!(f, "{}", first_byte as char)?;
            } else {
                write!(f, "\\{:#X}", first_byte)?;
            }

            for &b in &self.0[1..] {
                if b >= b' ' && b <= b'~' {
                    write!(f, "{}", b as char)?;
                } else {
                    write!(f, "\\{:#X}", b)?;
                }
            }
        }

        write!(f, "\")")?;

        Ok(())
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum VarBindValue {
    Integer(i32),
    OctetString(OctetString),
    ObjectId(ObjectId),
    IpAddress([u8; 4]),
    Counter32(u32),
    Unsigned32(u32),
    Gauge32(u32),
    TimeTicks(u32),
    Opaque(Vec<u8>),
    Counter64(u64),
    Null,
    Unspecified,
    NoSuchObject,
    NoSuchInstance,
    EndOfMibView,
}

impl<'b, 'a: 'b> DerDecode<'b, 'a> for VarBindValue {
    fn decode_from(buffer: &'b mut &'a [u8]) -> Result<Self, DecodingError> {
        use std::convert::TryInto;

        let id = buffer.get(0).ok_or_else(|| {
            DecodingError::IoError(io::Error::new(
                io::ErrorKind::UnexpectedEof,
                "Unexpected end of buffer",
            ))
        })?;

        #[allow(unreachable_patterns)]
        Ok(match *id {
            ASN1_INTEGER_RAW_TAG => VarBindValue::Integer(Integer::decode_from(buffer)?.0 as i32),
            ASN1_OCTET_STRING_RAW_TAG => {
                VarBindValue::OctetString(OctetString::decode_from(buffer)?)
            }
            ASN1_OBJECT_IDENTIFIER_RAW_TAG => {
                VarBindValue::ObjectId(ObjectId::decode_from(buffer)?)
            }
            n @ IP_ADDRESS_ID..=COUNTER64_ID => match n {
                IP_ADDRESS_ID => {
                    buffer.read_u8()?;
                    let len = Length::decode_from(buffer)?.0;

                    if len != 4 {
                        return Err(DecodingError::InvalidLength(len));
                    }

                    let mut ip = [0; 4];
                    buffer.read_exact(&mut ip)?;
                    VarBindValue::IpAddress(ip)
                }
                COUNTER32_ID => {
                    buffer.read_u8()?;
                    let len = Length::decode_from(buffer)?.0 as usize;
                    let val = buffer.read_uint::<BigEndian>(len)?.try_into()?;

                    VarBindValue::Counter32(val)
                }
                UNSIGNED32_ID => {
                    buffer.read_u8()?;
                    let len = Length::decode_from(buffer)?.0 as usize;
                    let val = buffer.read_uint::<BigEndian>(len)?.try_into()?;

                    VarBindValue::Unsigned32(val)
                }
                GAUGE32_ID => {
                    buffer.read_u8()?;
                    let len = Length::decode_from(buffer)?.0 as usize;
                    let val = buffer.read_uint::<BigEndian>(len)?.try_into()?;

                    VarBindValue::Gauge32(val)
                }
                TIME_TICKS_ID => {
                    buffer.read_u8()?;
                    let len = Length::decode_from(buffer)?.0 as usize;
                    let val = buffer.read_uint::<BigEndian>(len)?.try_into()?;

                    VarBindValue::TimeTicks(val)
                }
                OPAQUE_ID => {
                    buffer.read_u8()?;
                    let len = Length::decode_from(buffer)?.0 as usize;

                    let mut val = vec![0; len];
                    buffer.read_exact(&mut val)?;

                    VarBindValue::Opaque(val)
                }
                COUNTER64_ID => {
                    buffer.read_u8()?;
                    let len = Length::decode_from(buffer)?.0 as usize;
                    let val = buffer.read_uint::<BigEndian>(len)?;

                    VarBindValue::Counter64(val)
                }
                _ => return Err(DecodingError::InvalidObjectSyntaxId),
            },
            _ => return Err(DecodingError::InvalidObjectSyntaxId),
        })
    }
}

impl DerEncode for VarBindValue {
    fn encoded_size(&self) -> usize {
        use self::VarBindValue::*;

        match self {
            IpAddress(ip) => 1 + ip.encoded_size(),
            Counter32(i) => 1 + i.encoded_size(),
            Unsigned32(i) => 1 + i.encoded_size(),
            Gauge32(i) => 1 + i.encoded_size(),
            TimeTicks(i) => 1 + i.encoded_size(),
            Opaque(d) => 1 + d.encoded_size(),
            Counter64(i) => 1 + i.encoded_size(),
            Integer(i) => 1 + i.encoded_size(),
            ObjectId(oid) => oid.encoded_size(),
            OctetString(s) => s.encoded_size(),
            Null => 1 + Length::new(0).encoded_size(),
            _ => unimplemented!(),
        }
    }

    fn encode_into<W: Write>(&self, buffer: &mut W) -> io::Result<()> {
        use self::VarBindValue::*;

        match self {
            IpAddress(ip) => {
                buffer.write_u8(IP_ADDRESS_ID)?;
                ip.encode_into(buffer)?;
            }
            Counter32(i) => {
                buffer.write_u8(COUNTER32_ID)?;
                i.encode_into(buffer)?;
            }
            Unsigned32(i) => {
                buffer.write_u8(COUNTER32_ID)?;
                i.encode_into(buffer)?;
            }
            Gauge32(i) => {
                buffer.write_u8(COUNTER32_ID)?;
                i.encode_into(buffer)?;
            }
            TimeTicks(i) => {
                buffer.write_u8(TIME_TICKS_ID)?;
                i.encode_into(buffer)?;
            }
            Opaque(d) => {
                buffer.write_u8(OPAQUE_ID)?;
                d.encode_into(buffer)?;
            }
            Counter64(i) => {
                buffer.write_u8(COUNTER64_ID)?;
                i.encode_into(buffer)?;
            }
            Integer(i) => {
                buffer.write_u8(ASN1_INTEGER_RAW_TAG)?;
                i.encode_into(buffer)?;
            }
            ObjectId(oid) => oid.encode_into(buffer)?,
            OctetString(s) => s.encode_into(buffer)?,
            Null => {
                buffer.write_u8(ASN1_NULL_RAW_TAG)?;
                Length::new(0).encode_into(buffer)?;
            }
            _ => unimplemented!(),
        }

        Ok(())
    }
}
