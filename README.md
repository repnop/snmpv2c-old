# `snmpv2c`

An SNMPv2C library for encoding/decoding [SNMP](https://en.wikipedia.org/wiki/Simple_Network_Management_Protocol) packets along with sending them on the network.